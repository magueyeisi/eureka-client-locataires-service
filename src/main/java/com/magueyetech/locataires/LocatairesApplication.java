package com.magueyetech.locataires;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LocatairesApplication {

	public static void main(String[] args) {
		SpringApplication.run(LocatairesApplication.class, args);
	}

}
