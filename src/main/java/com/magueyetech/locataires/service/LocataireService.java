package com.magueyetech.locataires.service;

import java.util.List;

import com.magueyetech.locataires.domain.Locataire;
import com.magueyetech.locataires.repository.LocataireRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service implementation for ILocataire
 **/
@Service
public class LocataireService implements ILocataire {

    @Autowired
    private LocataireRepository locataireRepository;

    @Override
    public List<Locataire> findAll() {
        return (List<Locataire>) locataireRepository.findAll();
    }

    @Override
    public Locataire get(Integer id) {
        return locataireRepository.findById(id).get();
    }

    @Override
    public Locataire save(Locataire locataire) {
        return locataireRepository.saveAndFlush(locataire);
    }

    @Override
    public Locataire update(Locataire locataire) {
        return save(locataire);
    }

    @Override
    public boolean delete(Integer id) {
        locataireRepository.deleteById(id);
        return !locataireRepository.existsById(id);
    }

}
