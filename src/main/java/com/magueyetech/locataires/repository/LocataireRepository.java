package com.magueyetech.locataires.repository;

import com.magueyetech.locataires.domain.Locataire;

import org.springframework.data.jpa.repository.JpaRepository;

public interface LocataireRepository extends JpaRepository<Locataire, Integer> {

}
