package com.magueyetech.locataires.domain;

import java.util.List;

/**
 * Class for Menu Manager
 *
 * @author Papa Magueye GUEYE
 */
public class MenuManager {
    private int id;
    private String name;

    private List<MenuManager> child;

    public MenuManager(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public MenuManager(int id, String name, List<MenuManager> child) {
        this.id = id;
        this.name = name;
        this.child = child;
    }

    /**
     * @return int
     */
    public int getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return String
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return List<MenuManager>
     */
    public List<MenuManager> getChild() {
        return child;
    }

    public void setChild(List<MenuManager> child) {
        this.child = child;
    }

}
