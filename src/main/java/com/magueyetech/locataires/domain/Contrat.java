package com.magueyetech.locataires.domain;

import java.sql.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Represents a contrat.
 *
 * @author Papa Magueye GUEYE - MagueyeTech
 */
@Entity
@Table(name = "contrats")
public class Contrat {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String numero;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date date;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date debut;

    private int duree;
    private Float montantLoyer;
    private String status;

    @ManyToOne
    @JoinColumn(name = "local_id", nullable = true)
    private Local local;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "locataire_id", nullable = true)
    @JsonIgnoreProperties(value = { "contrats" })
    private Locataire locataire;

    /**
     * @return Integer
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return String
     */
    public String getNumero() {
        return numero;
    }

    /**
     * @param numero
     */
    public void setNumero(String numero) {
        this.numero = numero;
    }

    /**
     * @return Date
     */
    public Date getDate() {
        return date;
    }

    /**
     * @param date
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * @return Date
     */
    public Date getDebut() {
        return debut;
    }

    /**
     * @param debut
     */
    public void setDebut(Date debut) {
        this.debut = debut;
    }

    /**
     * @return int
     */
    public int getDuree() {
        return duree;
    }

    /**
     * @param duree
     */
    public void setDuree(int duree) {
        this.duree = duree;
    }

    /**
     * @return Float
     */
    public Float getMontantLoyer() {
        return montantLoyer;
    }

    /**
     * @param montantLoyer
     */
    public void setMontantLoyer(Float montantLoyer) {
        this.montantLoyer = montantLoyer;
    }

    /**
     * @return String
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return Local
     */
    public Local getLocal() {
        return local != null ? local : null;
    }

    /**
     * @param local
     */
    public void setLocal(Local local) {
        this.local = local;
    }

    /**
     * @return Locataire
     */
    public Locataire getLocataire() {
        return locataire != null ? locataire : null;
    }

    /**
     * @param locataire
     */
    public void setLocataire(Locataire locataire) {
        this.locataire = locataire;
    }

}
